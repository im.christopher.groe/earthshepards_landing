
var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{3})+$/;

export const checkEmailFormat = (email) => {
    return email.match(mailformat);
}